import sys
import re
import random

ENDS = ['.']
MAX_LENGTH = 100

with open(sys.argv[-1]) as file:
    lines = str.join('\n', file.readlines())
    words = []
    for line in [lines]:
        words += re.split(r'([.,;!?\"()\n])|\s+', line)

    words = list(filter(None, words))
    table = {}
    starters = [words[0].lower()]
    for i in range(0, len(words) - 1):
        here = words[i].lower()
        next = words[i + 1].lower()
        if here not in table: table[here] = []
        if next not in table[here]: table[here].append(next)

        if here in ENDS:
            if next not in starters: starters.append(next)
            table[here].append(0)

    result = []
    current = random.choice(starters)
    while current != 0:
        if current == '\n': result.append(current)
        else: result.append(current + ' ')
        if len(result) >= MAX_LENGTH: break
        current = random.choice(table[current])

    print()
    print(str.join('', result))